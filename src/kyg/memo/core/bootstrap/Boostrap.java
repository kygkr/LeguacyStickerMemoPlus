package kyg.memo.core.bootstrap;

import kyg.memo.resources.RS;
import kyg.memo.view.MainFrame;

public class Boostrap {
	public static void main(String[] args) {
		Log.init(RS.NAME);
		MainFrame.init();
	}
}
