package kyg.memo.core.bootstrap;

public class Log {

	private static String name;

	public static void init(String str) {
		name = str;
		i("Logger init");
	}

	public static void i(String str) {
		exeLog("[" + name + "/INFO] " + str);
	}

	public static void w(String str) {
		exeLog("[" + name + "/WARNING] " + str);
	}

	public static void e(String str) {
		exeLog("[" + name + "/ERROR] " + str);
	}
	
	private static void exeLog(String str) {
		if(name == null)
			System.out.println("[LOGGER/ERROR] 로그가 초기화되지 않았습니다.");
		else
			System.out.println(str);
	}
}
