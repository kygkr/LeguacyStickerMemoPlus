package kyg.memo.test.view;

import java.awt.TextField;

import javax.swing.JFrame;
import javax.swing.JPanel;

import kyg.memo.core.Calculate;
import kyg.memo.core.bootstrap.Log;
import kyg.memo.resources.RS;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {
	private JTextField input;
	private JTextField output;

	public MainFrame() {
		this.setSize(600, 400);
		this.setTitle(RS.NAME);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		input = new JTextField();
		input.setColumns(99);

		output = new JTextField();
		output.setColumns(10);

		JButton btnTest = new JButton("test");
		btnTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Log.i("테스트 버튼 클릭");
				output.setText(Calculate.calculate(input.getText()));
			}
		});

		JButton btnCancel = new JButton("canel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Log.i("모든 텍스트를 제거합니다.");
				input.setText("");
			}
		});

		GroupLayout group = new GroupLayout(getContentPane());
		group.setHorizontalGroup(group.createParallelGroup(Alignment.TRAILING)
				.addComponent(input, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
				.addGroup(group.createSequentialGroup().addContainerGap(295, Short.MAX_VALUE).addComponent(btnTest)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnCancel).addContainerGap())
				.addComponent(output, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE));
		group.setVerticalGroup(
				group.createParallelGroup(Alignment.LEADING)
						.addGroup(
								group.createSequentialGroup()
										.addComponent(input, GroupLayout.PREFERRED_SIZE, 157,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(group.createParallelGroup(Alignment.BASELINE).addComponent(btnCancel)
												.addComponent(btnTest))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(output, GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
										.addContainerGap()));
		getContentPane().setLayout(group);

		this.setVisible(true);
	}
}
