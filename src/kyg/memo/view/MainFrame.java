package kyg.memo.view;

import javax.swing.JFrame;

import kyg.memo.resources.RS;
import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextPane;

public class MainFrame extends JFrame{

	public static void init() {
		new MainFrame();
	}
	
	private MainFrame() {
		this.setSize(180, 165);
		this.setTitle(RS.NAME);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTextPane textPane = new JTextPane();
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
		);
		getContentPane().setLayout(groupLayout);
		
		this.setVisible(true);
	}
}
